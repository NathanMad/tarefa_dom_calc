document.addEventListener('DOMContentLoaded', () => {
    let counter = 0;
    document.querySelector('#set-grade').addEventListener('click', (e) => {
        e.preventDefault();
        if (document.querySelector('#grade').value == '') {
            alert('Por favor, insira um número');
        } else if (!(document.querySelector('#grade').value > 10) && !(document.querySelector('#grade').value < 0)) {
            let value = document.createElement('p');
            value.innerHTML = `
            A nota ${counter += 1} foi <span class='grades'>${document.querySelector('#grade').value}</span>`;
            document.querySelector('fieldset').appendChild(value);
        } else {
            alert('Por favor, insira um número entre 0 e 10 (inclusivo)');
        }
    });
    document.querySelector('input[type=submit]').addEventListener('click', (e) => {
        e.preventDefault();
        if (document.querySelector('#result span')) {
            document.querySelector('#result span').innerText = '';
        }
        let summation = 0;
        for (let grade of document.querySelectorAll('fieldset .grades')) {
            summation = +summation + +grade.innerText;
        }
        summation = Math.round(summation * 100) / 100;
        let result = document.createElement('span');
        result.style = 'font-weight: bold';
        result.innerHTML = `&nbsp;${Math.round(summation / document.querySelectorAll('fieldset .grades').length * 100) / 100}`;
        document.querySelector('#result').appendChild(result);
        document.querySelector('fieldset').innerHTML = '';
        counter = 0;
    })
})